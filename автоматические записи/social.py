# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re, logging
logging.basicConfig(format = u'%(levelname)-8s [%(asctime)s] %(message)s', level = logging.DEBUG, filename = u'mylog.log')

class Social(unittest.TestCase, driver, base_url):
    def setUp(self):
        self.driver = driver

    def test_social(self):
        driver = self.driver
        driver.get(self.base_url + "/account/login")
        try:
            element = driver.find_element_by_id("Vkontakte")
            element.click()
            try:
                assert("ВКонтакте|Вход" in driver.title)
            except AssertionError:
                logging.error("Could not open VK login page")
        except NoSuchElementException:
            logging.error("No VK login element on page")
        try:
            element = driver.find_element_by_id("Google")
            element.click()
            try:
                assert ("Вход" in driver.title)
            except AssertionError:
                logging.error("Could not open Google login page")
        except NoSuchElementException:
            logging.error("No Google login element on page")

    
    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
