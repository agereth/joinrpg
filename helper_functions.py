import unittest
import logging
from settings import gecko_path
from settings import base_url
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException



# ToDo: move logger and it's settings to separate file
logger = logging.getLogger('JoinRPG tests')
logger.setLevel(logging.DEBUG)

fh = logging.FileHandler('logfile.log')
fh.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)

formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)

# add the handlers to logger
logger.addHandler(ch)
logger.addHandler(fh)

class Page(unittest.TestCase):
    """
    Contains functions, applicable for all pages of the site
    Parent class for specific pages
    """

    @classmethod
    def setUpClass(cls):
        """
        Initialize Selenium driver for tests
        :return:
        """
        try:
            logger.info("Trying to set up Webdriver")
            cls.driver = webdriver.Firefox(executable_path=gecko_path)
            logger.info("Webdriver started.")
            cls.driver.get(base_url)
        except:
            logger.exception("Exception starting Webdriver")

    def check_title(self,expected_title):
        self.driver.get(base_url)
        try:
            logger.info("check_title called.")
            assert expected_title in self.driver.title
        except AssertionError:
            logging.error("check_title: Wrong page title. Actual title: %s. Expected title: %s", self.driver.title, expected_title)
        except:
            logging.exception("check_title")

#   function for forgotten password link
    def check_forgottenpassword(self, mailaddress):
        logger.info("Check_forgotten_password called")
        self.driver.get(base_url + "/account/login")
        try:
            forget_link = self.driver.find_element_by_link_text(u"Забыли пароль?")
            forget_link.click()
            logger.info(self.driver.title)
            try:
                assert "Забыли пароль?" in self.driver.title
                try:
                    email = self.driver.find_element_by_id("Email")
                    email.clear()
                    email.send_keys(mailaddress)
                    try:
                        button = self.driver.find_element_by_css_selector("input.btn.btn-default")
                        button.click()
                        try:
                            assert "Восстановление пароля" in self.driver.title
                        except AssertionError:
                            logging.error("Something wrong with password reset")
                    except NoSuchElementException:
                        logging.error("No reset password link")
                except NoSuchElementException:
                    logging.error("No e-mail field")
            except AssertionError:
                logging.error("Wrong reset password page")
        except NoSuchElementException:
            logging.error("No reset password link")
        except:
            logging.exception("forgotten_password")

    @classmethod
    def tearDownClass(cls):
        """
        Close Selenium Driver
        :return:
        """
        cls.driver.close()
        logger.info("Webdriver closed.")